var express = require('express');
var router = express.Router();
var products = require('../data/products');
var orderservice = require('../services/order-service');

function prepareOrderDetails(order) {
    var prods = Object.keys(order).map(function (key) {
        var product = products.filter(function (product) {
            return product.id == key;
        })[0];

        return {
            name: product.name,
            id: product.id,
            price: product.price,
            quantity: order[key],
            cost: order[key] * product.price

        };
    });

    return {
        products: prods,
        total: prods.reduce(function (memo, product) {
            return memo + product.cost
        }, 0)
    };
}

router.route('/')
    .get(function (req, res) {
        res.render('cart', prepareOrderDetails(JSON.parse(req.query.order)));
    })
    .post(function (req, res) {
        var order = prepareOrderDetails(JSON.parse(req.query.order));
        order.email = req.query.email;
        order.phone = req.query.phone;
        var orderId = orderservice.publish(order);
        res.json({orderId: orderId});
    });

module.exports = router;
