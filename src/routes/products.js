var express = require('express');
var router = express.Router();
var products = require('../data/products');

router.get('/', function (req, res) {
    res.render('products', {products: products});
});

module.exports = router;
