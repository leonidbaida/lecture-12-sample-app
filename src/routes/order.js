var express = require('express');
var router = express.Router();
var orderservice = require('../services/order-service');

router.get('/:orderId', function (req, res) {
    res.render('order', orderservice.get(req.params.orderId));
});

module.exports = router;