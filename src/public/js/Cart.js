define(function () {

    function CartService() {
        if (!localStorage.getItem('cart')) {
            this._data = {};
            this._save();
        }

        try {
            this._data = JSON.parse(localStorage.getItem('cart'));
        } catch (e) {
            this._data = {};
        }
    }

    CartService.prototype = Object.create({
        _save: function () {
            localStorage.setItem('cart', this.serialize());
        },

        serialize: function () {
            return JSON.stringify(this._data);
        },

        add: function (id, quantity) {
            if (quantity === 0) {
                this.remove(id);

            } else {
                this._data[id] = quantity || 1;
                this._save();
            }
        },

        remove: function (id) {
            delete this._data[id];
            this._save();
        },

        count: function () {
            return Object.keys(this._data).length;
        },

        has: function (id) {
            return this._data.hasOwnProperty(id);
        },
        removeAll: function () {
            this._data = {};
            this._save();
        }
    });

    CartService.prototype.constructor = CartService;

    return new CartService();
});